# -*- coding: utf-8 -*-
"""Blondemail
Because the word needs another email module.
For now, uses smtplib. The reaon this exists is that I might one day
want to shift all my scripts to use something else, so may as well
modularise it.
"""

import smtplib
from email.mime.text import MIMEText

class Blondemail(object):
    def __init__(self, server, port, username, password):
        self.s = smtplib.SMTP_SSL(server, port)
        self.s.login(username, password)

    def send(self, from_email, to_email, subject, message):
        try:
            msg = MIMEText(message, 'html')
            msg['Subject'] = subject
            msg['From'] = from_email
            msg['To'] = to_email
            self.s.sendmail(from_email, [to_email], msg.as_string())
        except Exception, e:
            print "Blondemail failed: %s" % e

    def close(self):
        self.s.quit()
