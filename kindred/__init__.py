# -*- coding: utf-8 -*-
"""Kindred
For doing the Kindred things.
"""
import os
import re
import markdown

def loadFiles(path, ignored):
    """ Load a list of all filenames from a given path, removing and from a list
    of ignored filenames
    """
    files = os.listdir(path)
    for i in ignored:
        try:
            files.remove(i)
        except Exception:
            pass
    return files


def countInstances(path, filename, match_string):
    """ Counts the instances of a particular string in a file, given a filename
    """
    file = "%s/%s" % (path, filename)
    with open(file,'r') as f_open:
        data = f_open.read()
        return data.count(match_string)


def retrieveChunk(path, filename, index, delimiter):
    """ Retrieves a chunk of text from a given path/filename. The chunk of 
    text is that which comes after the index'd count of delimiter, and 
    finishes at the next occurence of delimiter or end of the file.
    """
    file = "%s/%s" % (path, filename)
    with open(file,'r') as f_open:
        data = f_open.read()
        locs = [i for i in range(len(data)) if data.startswith(delimiter, i)]
        locs.append(len(data))
        chunk = data[locs[index]+len(delimiter)+1:locs[index+1]]
        return markdown.markdown(unicode(chunk, 'utf-8', errors='ignore'))


def retrieveTitle(path, filename):
    """ Retrieves the title of a file, assuming the title is the first line.
    Returns the title stripped of markdown heading markers (#)
    """
    file = "%s/%s" % (path, filename)
    with open(file,'r') as f_open:
        title = f_open.readline().lstrip("#").strip()
    return title


def markdownFile(path, filename):
    """ Retrieves the given file, and returns the entire contents formatted as
    html
    """
    file = "%s/%s" % (path, filename)
    with open(file,'r') as f_open:
        data = f_open.read()
        data = duplicateLinebreaks(data)
        html = markdown.markdown(unicode(data, 'utf-8', errors='ignore'))
        return html

def duplicateLinebreaks(data):
    """ Searches for single line breaks and converts them to double line
    breaks, so that markdown renders nicely.
    """
    linebreaks = re.finditer(r".(\n).", data)
    offset = 1
    for l in linebreaks:
        pos = l.start(0)+offset
        data = data[:pos] + "\n" + data[pos:]
        offset += 1
    return data
