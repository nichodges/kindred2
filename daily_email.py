# -*- coding: utf-8 -*-
""" Daily Email
Sends a random highlight as an email. 
"""

import os
import random
import socket
from ConfigParser import SafeConfigParser
import kindred
from Blondemail import Blondemail

def main():
    # Load config
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    parser = SafeConfigParser()
    parser.read('config.ini')
    if socket.gethostname() in [e.strip() for e in parser.get(
                                    'meta', 'dev_servers').split(',')]:
        env = "dev"
    elif socket.gethostname() in [e.strip() for e in parser.get(
                                    'meta', 'production_servers').split(',')]:
        env = "production"
    else:
        exit("Current environment not in config")
    # Set Config vars
    highlights_path = parser.get(env, 'highlights_path')
    email_server = parser.get(env, 'email_server')
    email_port = int(parser.get(env, 'email_port'))
    email_username = parser.get(env, 'email_username')
    email_password = parser.get(env, 'email_password')
    email_to = parser.get(env, 'email_to')
    email_from = parser.get(env, 'email_from')
    ignored_files = [e.strip() for e in parser.get(
                                        env, 'ignore_files').split(',')]
    ignored_files.append(".git")
    delimiter = parser.get(env, 'delimiter')
    highlight_full = 0

    # Get list of files
    files = kindred.loadFiles(highlights_path, ignored_files)
    # Find out how many highlights in each file, and create a big list of all
    highlights_index = []
    for f in files:
        count = kindred.countInstances(highlights_path, f, delimiter)
        for c in range(count):
            highlights_index.append({f:c})

    while highlight_full == 0:
        # Select one at random
        selected = highlights_index[random.randint(0,len(highlights_index)-1)]
        file = selected.keys()[0]
        index = selected[file]
        # Grab that highlight (remember 0 index!)
        chunk = kindred.retrieveChunk(highlights_path, file, index, delimiter)
        if len(chunk) < 3:
            pass
        else:
            highlight_full = 1
            title = kindred.retrieveTitle(highlights_path, file)
            # Send away!
            chunk = chunk.replace("<p>", "<p style='font-family: \"Georgia\"; color: #333333; font-size: 14px; line-height: 20px;'>")
            msg = "<table><tr><td width='10%'>&nbsp;</td><td>"
            msg += "<h3 style='font-family: \"Georgia\"; color: #000000; text-align: center; font-size: 18px; text-decoration: underline;'>%s</h3>%s" % (title, chunk)
            msg += "</td><td width='10%'>&nbsp;</td></tr></table>"
            bm = Blondemail(email_server, email_port, email_username, email_password)
            bm.send(email_from, email_to, "Your daily Kindred highlight", msg.encode('utf8'))
            bm.close()


if __name__ == "__main__":
    main()