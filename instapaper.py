# -*- coding: utf-8 -*-
""" Instapaper
Loads up a random markdown file with highlights, and emails is to the 
instapaper-add email supplied in config.
"""
import os
import random
import socket
from ConfigParser import SafeConfigParser
import kindred
from Blondemail import Blondemail

def main():
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    parser = SafeConfigParser()
    parser.read('config.ini')
    # Check for environment...
    if socket.gethostname() in [e.strip() for e in parser.get(
                                    'meta', 'dev_servers').split(',')]:
        env = "dev"
    elif socket.gethostname() in [e.strip() for e in parser.get(
                                    'meta', 'production_servers').split(',')]:
        env = "production"
    else:
        exit("Current environment not in config")
    # Set Config vars...
    highlights_path = parser.get(env, 'highlights_path')
    email_server = parser.get(env, 'email_server')
    email_port = int(parser.get(env, 'email_port'))
    email_username = parser.get(env, 'email_username')
    email_password = parser.get(env, 'email_password')
    email_from = parser.get(env, 'email_to')
    ignored_files = [e.strip() for e in parser.get(
                                        env, 'ignore_files').split(',')]
    ignored_files.append(".git")
    instapaper_email = parser.get(env, 'instapaper_email')
    # OK Go...
    files = kindred.loadFiles(highlights_path, ignored_files)
    selected = files[random.randint(0,len(files)-1)]
    message = kindred.markdownFile(highlights_path, selected).encode('utf-8').strip()
    message = message.replace("<hr />", "<p><center>&#8226; &#8226; &#8226;</center></p>")
    message = message.replace("<p><center>&#8226; &#8226; &#8226;</center></p>", "", 1)
    subject = "Kindred Highlights: %s" % kindred.retrieveTitle(highlights_path, selected)
    # And mail...
    bm = Blondemail(email_server, email_port, email_username, email_password)
    bm.send(email_from, instapaper_email, subject, message)
    bm.close()


if __name__ == "__main__":
    main()
