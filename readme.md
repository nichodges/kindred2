# Kindred2
Kindred2 is a tool to make the most of book highlights. Kindred2 uses a simple database of markdown files containing notes from your books, and delivers three functions:

1. A daily email with a random quote from a random book
2. A weekly addition of an entire book's notes into an Instapaer account
3. A basic web interface to view and search all book highlights (coming soon!)

Why is it called Kindred2? Because the original was an app called Kindred, built in PHP, that would automatically grab Kindle highlights from your Kindle account and give you a nice view. But Amazon didn't love that.

---
## Todo
- [ ] Make email styling super nice
- [ ] Build interface with navigation and search (nice styling and permalinks)
- [ ] Link in daily email to permalink in web interface
- [ ] Link Instapaper to a public highlights page

---

## Installation
There's not a lot to this, rename config.ini.sample to config.ini and add in your email details. Mail is sent using smtplib but if you want to go any fancier than that feel free to replace to Blondemail module.

### How to sort your highlights
Kindred2 assumes a certain structure of how you store highlights. Essentially it looks in a directory (highlights_path in config.ini) and grabs all files (apart from those listed in ignore_files in config.ini). It is assumed each file is one book worth of highlights. Filenames can be whatever you want, but the actual file should look something like this:
```
# Book Title - Book Author
- Read: December 2016

## Notes
Any notes you want to write about this book. This can include any Markdon formatting you want like
> blockquotes

and the like. Basically you can write anything you want before the first *** and it will be treated as notes by the Kindred2 scripts.

## Highlights
***
This would be your first book highlight.

***
And second.

Which may be several paragraphs.

***
And you should always finish with a line of ***, but the scripts won't fail if you don't.

***
```

I use a (private) Git repository for my highlights - so when I add a new book I just have to commit and push to the server where Kindred2 is running, and that book will now be included in all scripts.

### Daily Email (daily_email.py)
While this is named daily_email.py, it actually just sends an email with a random book highlight. Logically you set it as a cronjob to run daily.

### Instapaper (instapaper.py)
This will send an entire book's notes and highlights to your Instapaper "read later" email address. Because Instapaper ignores `<hr>`'s, it will first replace all of these with a nice centred ellipsis. Run this whenever you want a book's notes and highlights added to your Instapaper. 

